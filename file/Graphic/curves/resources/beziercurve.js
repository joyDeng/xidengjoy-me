// Class definition for a Bezier Curve
var BezierCurve = function(canvasId, ctx)
{
    // Setup all the data related to the actual curve.
    this.nodes = new Array();
    this.showControlPolygon = true;
    this.showAdaptiveSubdivision = false;
    this.tParameter = 0.5;
    this.tDepth = 2;
    
    // Set up all the data related to drawing the curve
    this.cId = canvasId;
    this.dCanvas = document.getElementById(this.cId);
    if (ctx) {
        this.ctx = ctx;
        return;
    } else {
        this.ctx = this.dCanvas.getContext('2d');
    }
    this.computeCanvasSize();
    
    // Setup event listeners
    this.cvState = CVSTATE.Idle;
    this.activeNode = null;
    
    // closure
    var that = this;
    
    // Event listeners
    this.dCanvas.addEventListener('resize', this.computeCanvasSize());
    
    this.dCanvas.addEventListener('mousedown', function(event) {
        that.mousePress(event);
    });
    
    this.dCanvas.addEventListener('mousemove', function(event) {
        that.mouseMove(event);
    });
    
    this.dCanvas.addEventListener('mouseup', function(event) {
        that.mouseRelease(event);
    });
    
    this.dCanvas.addEventListener('mouseleave', function(event) {
        that.mouseRelease(event);
    });
}

// Mutator methods.
BezierCurve.prototype.setT = function(t)
{
    this.tParameter = t;
}

BezierCurve.prototype.setDepth = function(d)
{
    this.tDepth = d;
}

BezierCurve.prototype.setShowControlPolygon = function(bShow)
{
    this.showControlPolygon = bShow;
}

BezierCurve.prototype.setShowAdaptiveSubdivision = function(bShow)
{
    this.showAdaptiveSubdivision = bShow;
}

// Event handlers for the canvas associated with the curve.
BezierCurve.prototype.mousePress = function(event)
{
    if (event.button == 0) {
        this.activeNode = null;
        var pos = getMousePos(event);

        // Try to find a node below the mouse
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].isInside(pos.x,pos.y)) {
                this.activeNode = this.nodes[i];
                break;
            }
        }
    }

    // No node selected: add a new node
    if (this.activeNode == null) {
        this.addNode(pos.x,pos.y);
        this.activeNode = this.nodes[this.nodes.length-1];
    }

    this.cvState = CVSTATE.SelectPoint;
    event.preventDefault();
}

BezierCurve.prototype.mouseMove = function(event) {
    if (this.cvState == CVSTATE.SelectPoint || this.cvState == CVSTATE.MovePoint) {
        var pos = getMousePos(event);
        this.activeNode.setPos(pos.x,pos.y);
    } else {
        // No button pressed. Ignore movement.
    }
}

BezierCurve.prototype.mouseRelease = function(event)
{
    this.cvState = CVSTATE.Idle; this.activeNode = null;
}

// Some util functions.
BezierCurve.prototype.computeCanvasSize = function() 
{
    var renderWidth = Math.min(this.dCanvas.parentNode.clientWidth - 20, 820);
    var renderHeight = Math.floor(renderWidth*9.0/16.0);
    this.dCanvas.width = renderWidth;
    this.dCanvas.height = renderHeight;
}

BezierCurve.prototype.drawControlPolygon = function()
{
    for (var i = 0; i < this.nodes.length-1; i++)
        drawLine(this.ctx, this.nodes[i].x, this.nodes[i].y,
                           this.nodes[i+1].x, this.nodes[i+1].y);
}

BezierCurve.prototype.drawControlPoints = function()
{
    for (var i = 0; i < this.nodes.length; i++)
        this.nodes[i].draw(this.ctx);
}

// TODO: Task 1 - Implement the De Casteljau split, given a parameter location 't' about which the curve should be split.
BezierCurve.prototype.deCasteljauSplit = function(t)
{
    // Create left and right childs and pass the correct contexts.
    var left = new BezierCurve(this.cId, this.ctx);
    var right = new BezierCurve(this.cId, this.ctx);

    if (this.nodes.length == 3)
    {
        // degree 2 bezier curve
        // split the segments about 't'
        // Hint : use lerp()
        var split = new Node();
        var splitleft = new Node();
        var splitright = new Node();

        splitleft.lerp(this.nodes[0],this.nodes[1],t);
        splitright.lerp(this.nodes[1],this.nodes[2],t);
        split.lerp(splitleft,splitright,t);

        left.nodes.push(this.nodes[0]);
        left.nodes.push(splitleft);
        left.nodes.push(split);
        right.nodes.push(this.nodes[2]);
        right.nodes.push(splitright);
        right.nodes.push(split);

    }
    else if (this.nodes.length == 4)
    {    
        // degree 3 bezier curve
        var degreeone1 = new Node();
        var degreeone2 = new Node();
        var degreeone3 = new Node();
        var degreetwo1 = new Node();
        var degreetwo2 = new Node();
        var split = new Node();

        degreeone1.lerp(this.nodes[0],this.nodes[1],t)
        degreeone2.lerp(this.nodes[1],this.nodes[2],t);
        degreeone3.lerp(this.nodes[2],this.nodes[3],t);
        degreetwo1.lerp(degreeone1,degreeone2,t);
        degreetwo2.lerp(degreeone2,degreeone3,t);
        split.lerp(degreetwo1,degreetwo2,t);

        left.nodes.push(this.nodes[0]);
        left.nodes.push(degreeone1);
        left.nodes.push(degreetwo1);
        left.nodes.push(split);

        right.nodes.push(this.nodes[3]);
        right.nodes.push(degreeone3);
        right.nodes.push(degreetwo2);
        right.nodes.push(split);
    }

    return {left: left, right: right};
}

// TODO: Task 2 - Implement the De Casteljau draw function.
BezierCurve.prototype.deCasteljauDraw = function(depth)
{
    // Check if depth == 0; if so draw the control polygon
    // Else get both children by split
    // recursively call draw on both children
    if(depth == 0){
        this.drawControlPolygon();
        //this.drawControlPoints();
    }
    else{
        
        this.deCasteljauSplit(this.tParameter).left.deCasteljauDraw(depth-1);
        
        this.deCasteljauSplit(this.tParameter).right.deCasteljauDraw(depth-1);
       
    }
}

// TODO: Task 3 - Implement the adaptive De Casteljau draw function
BezierCurve.prototype.adapativeDeCasteljauDraw = function()
{
    // NOTE: Only for graduate students
    // Compute a flatness measure.
    // If not flat, split and recurse on both
    // Else draw control vertices of the curve
 
     var p0p1 = new Node(this.nodes[1].x - this.nodes[0].x, this.nodes[1].y - this.nodes[0].y);
     var p0p3 = new Node(this.nodes[3].x - this.nodes[0].x, this.nodes[3].y - this.nodes[0].y);
     var p3p2 = new Node(this.nodes[2].x - this.nodes[3].x, this.nodes[2].y - this.nodes[3].y);

     var dot12 = p0p1.x * p0p3.x + p0p1.y * p0p3.y;
     var dot23 = - p3p2.x * p0p3.x - p3p2.y * p0p3.y;

     var dot11 = p0p1.x * p0p1.x + p0p1.y * p0p1.y;
     var dot22 = p0p3.x * p0p3.x + p0p3.y * p0p3.y;
     var dot33 = p3p2.x * p3p2.x + p3p2.y * p3p2.y;

     var cosa = dot12/(Math.sqrt(dot11) * Math.sqrt(dot22));
     var cosb = dot23/(Math.sqrt(dot33) * Math.sqrt(dot22));

     var d1 = Math.sqrt(dot11 - dot11 * cosa * cosa); 
     var d2 = Math.sqrt(dot33 - dot33 * cosb * cosb);
     var e = 2;

     if(Math.max(d1,d2) - 0 < e){
        this.drawControlPolygon();
        this.drawControlPoints();

     }else{
         this.deCasteljauSplit(this.tParameter).left.adapativeDeCasteljauDraw();
        this.deCasteljauSplit(this.tParameter).right.adapativeDeCasteljauDraw();

     }



}

// NOTE: Code for task 1
BezierCurve.prototype.drawTask1 = function()
{
    this.ctx.clearRect(0, 0, this.dCanvas.width, this.dCanvas.height);
    if(this.showControlPolygon)
    {
        // Connect nodes with a line
        setColors(this.ctx,'rgb(10,70,160)');
        this.drawControlPolygon();

        // Draw control points
        setColors(this.ctx,'rgb(10,70,160)','white');
        this.drawControlPoints();
    }
    
    if (this.nodes.length < 3)
        return;
    
    // De Casteljau split for one time
    var split = this.deCasteljauSplit(this.tParameter);
    setColors(this.ctx, 'red');
    split.left.drawControlPolygon();
    setColors(this.ctx, 'green');
    split.right.drawControlPolygon();
    
    setColors(this.ctx,'red','red');
    split.left.drawControlPoints();
    setColors(this.ctx,'green','green');
    split.right.drawControlPoints();
    
    // Draw the parameter value.
    drawText(this.ctx, this.nodes[0].x - 20,
                       this.nodes[0].y + 20,
                         "t = " + this.tParameter);
}

// NOTE: Code for task 2
BezierCurve.prototype.drawTask2 = function()
{
    this.ctx.clearRect(0, 0, this.dCanvas.width, this.dCanvas.height);
    
    if (this.showControlPolygon)
    {
        // Connect nodes with a line
        setColors(this.ctx,'rgb(10,70,160)');
        this.drawControlPolygon();

        // Draw control points
        setColors(this.ctx,'rgb(10,70,160)','white');
        this.drawControlPoints();
    }

    if (this.nodes.length < 3)
        return;
    
    // De-casteljau's recursive evaluation
    setColors(this.ctx,'black');
    this.deCasteljauDraw(this.tDepth);
}

// NOTE: Code for task 3
BezierCurve.prototype.drawTask3 = function()
{
    this.ctx.clearRect(0, 0, this.dCanvas.width, this.dCanvas.height);
    
    if (this.showControlPolygon)
    {
        // Connect nodes with a line
        setColors(this.ctx,'rgb(10,70,160)');
        this.drawControlPolygon();

        // Draw control points
        setColors(this.ctx,'rgb(10,70,160)','white');
        this.drawControlPoints();
    }

    if (this.nodes.length < 3)
        return;
    
    // De-casteljau's recursive evaluation
    setColors(this.ctx,'black');
    this.deCasteljauDraw(this.tDepth);
    
    // adaptive draw evaluation
    if(this.showAdaptiveSubdivision)
        this.adapativeDeCasteljauDraw();
}

// Add a contro point to the Bezier curve
BezierCurve.prototype.addNode = function(x,y)
{
    if (this.nodes.length < 4)
        this.nodes.push(new Node(x,y));
}
