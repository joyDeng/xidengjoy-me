 // Class definition for a Catmull-Rom spline
var CatmullRomSpline = function(canvasId)
{
    // Set up all the data related to drawing the curve
    this.cId = canvasId;
    this.dCanvas = document.getElementById(this.cId);
    this.ctx = this.dCanvas.getContext('2d');
    this.dCanvas.addEventListener('resize', this.computeCanvasSize());
    this.computeCanvasSize();
    
    // Setup all the data related to the actual curve.
    this.nodes = new Array();
    this.showControlPolygon = true;
    this.showTangents = true;
    
    // Assumes a equal parametric split strategy
    // In case of using Bezier De Casteljau code, add appropriate variables.
    this.numSegments = 16;
    
    // Global tension parameter
    // Undergrads - ignore this value.
    this.tension = 0.5;
    
    // Setup event listeners
    this.cvState = CVSTATE.Idle;
    this.activeNode = null;
    
    // closure
    var that = this;
    
    // Event listeners
    this.dCanvas.addEventListener('mousedown', function(event) {
        that.mousePress(event);
    });
    
    this.dCanvas.addEventListener('mousemove', function(event) {
        that.mouseMove(event);
    });
    
    this.dCanvas.addEventListener('mouseup', function(event) {
        that.mouseRelease(event);
    });
    
    this.dCanvas.addEventListener('mouseleave', function(event) {
        that.mouseRelease(event);
    });
}

// Mutator methods.
CatmullRomSpline.prototype.setShowControlPolygon = function(bShow)
{
    this.showControlPolygon = bShow;
}

CatmullRomSpline.prototype.setShowTangents = function(bShow)
{
    this.showTangents = bShow;
}

CatmullRomSpline.prototype.setTension = function(val)
{
    this.tension = val;
}

CatmullRomSpline.prototype.setNumSegments = function(val)
{
    this.numSegments = val;
}

// Event handlers.
CatmullRomSpline.prototype.mousePress = function(event)
{
    if (event.button == 0) {
        this.activeNode = null;
        var pos = getMousePos(event);

        // Try to find a node below the mouse
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].isInside(pos.x,pos.y)) {
                this.activeNode = this.nodes[i];
                break;
            }
        }
    }

    // No node selected: add a new node
    if (this.activeNode == null) {
        this.addNode(pos.x,pos.y);
        this.activeNode = this.nodes[this.nodes.length-1];
    }

    this.cvState = CVSTATE.SelectPoint;
    event.preventDefault();
}

CatmullRomSpline.prototype.mouseMove = function(event) {
    if (this.cvState == CVSTATE.SelectPoint || this.cvState == CVSTATE.MovePoint) {
        var pos = getMousePos(event);
        this.activeNode.setPos(pos.x,pos.y);
    } else {
        // No button pressed. Ignore movement.
    }
}

CatmullRomSpline.prototype.mouseRelease = function(event)
{
    this.cvState = CVSTATE.Idle; this.activeNode = null;
}

// Utility methods.
CatmullRomSpline.prototype.computeCanvasSize = function() 
{
    var renderWidth = Math.min(this.dCanvas.parentNode.clientWidth - 20, 820);
    var renderHeight = Math.floor(renderWidth*9.0/16.0);
    this.dCanvas.width = renderWidth;
    this.dCanvas.height = renderHeight;
}

CatmullRomSpline.prototype.drawControlPolygon = function()
{
    for (var i = 0; i < this.nodes.length-1; i++)
        drawLine(this.ctx, this.nodes[i].x, this.nodes[i].y,
                      this.nodes[i+1].x, this.nodes[i+1].y);
}

CatmullRomSpline.prototype.drawControlPoints = function()
{
    for (var i = 0; i < this.nodes.length; i++)
        this.nodes[i].draw(this.ctx);
}

// TODO: Task 4
CatmullRomSpline.prototype.drawTangents = function()
{
    // Note: Tangents are available only for 2,..,n-1 nodes. The tangent is not defined for 1st and nth node.
    // Compute tangents from (i+1) and (i-1) node
    // Normalize tangent and compute a line of length 'x' pixels from the current control point.
    // Draw the tangent using drawLine() function
    setColors(this.ctx,'rgb(255,0,0)','red');
    for(var i = 1;i < this.nodes.length-1; i++){
  
       var xl = this.nodes[i+1].x - this.nodes[i-1].x;
       var yl = this.nodes[i+1].y - this.nodes[i-1].y;
       var distance = Math.sqrt(Math.pow(yl,2) + Math.pow(xl,2));
       var sin = yl/(distance*2);
       var cos = xl/(distance*2);
       var x = 50.0;

       var point = new Node();
       point.x = this.nodes[i].x + cos * x;
       point.y = this.nodes[i].y + x * sin;


       drawLine(this.ctx, point.x,point.y,this.nodes[i].x, this.nodes[i].y);


    }

}

// TODO: Task 5
CatmullRomSpline.prototype.draw = function()
{
    //NOTE: You can either implement the equal parameter split strategy or recursive bezier draw for drawing the spline segments
    //NOTE: If you're a grad student, you will have to employ the tension parameter to draw the curve (see assignment description for more details)
    //Hint: Once you've computed the segments of the curve, draw them using the drawLine() function
        
    var s = this.tension;
   
   for(var i = 1; i < this.nodes.length-2; i++){
  
    var p0 = new Node(this.nodes[i-1].x,this.nodes[i-1].y);
    var p1 = new Node(this.nodes[i].x,this.nodes[i].y);
    var p2 = new Node(this.nodes[i+1].x, this.nodes[i+1].y);
    var p3 = new Node(this.nodes[i+2].x,this.nodes[i+2].y);
    
    var a0 = new Node(p1.x,p1.y);
    var a1 = new Node(s * (p2.x - p0.x) , s * (p2.y - p0.y));
    var a2 = new Node(s * (2 * p0.x + p1.x - 2 * p2.x - p3.x) + 3 * (p2.x - p1.x) , s * (2 * p0.y + p1.y - 2 * p2.y - p3.y) + 3 * (p2.y - p1.y));
    var a3 = new Node(s * (p2.x + p3.x - p0.x - p1.x) + 2 * (p1.x - p2.x) , s * (p2.y + p3.y - p0.y - p1.y) + 2 * (p1.y - p2.y));

    var point0 = new Node(this.nodes[i].x,this.nodes[i].y);
    for( var j = 1 ; j <= this.numSegments ; j++){
    	var u1 = (j/(this.numSegments*1.0));
    	var	u2 = Math.pow(j/(this.numSegments*1.0),2);
   		var u3 = Math.pow(j/(this.numSegments*1.0),3);
   		var point = new Node(a0.x  + u1 * a1.x + u2 * a2.x + u3 * a3.x , a0.y  + u1 * a1.y + u2 * a2.y + u3 * a3.y);
        
        setColors(this.ctx,'rgb(0,0,0)','black');
    	drawLine(this.ctx, point0.x , point0.y , point.x, point.y);
    	
        point0.x = point.x;
    	point0.y = point.y;
    }
 }

}

// NOTE: Task 4 code.
CatmullRomSpline.prototype.drawTask4 = function()
{
    // clear the rect
    this.ctx.clearRect(0, 0, this.dCanvas.width, this.dCanvas.height);
    
    if (this.showControlPolygon) {
        // Connect nodes with a line
        setColors(this.ctx,'rgb(10,70,160)');
        for (var i = 1; i < this.nodes.length; i++) {
            drawLine(this.ctx, this.nodes[i-1].x, this.nodes[i-1].y, this.nodes[i].x, this.nodes[i].y);
        }
        // Draw nodes
        setColors(this.ctx,'rgb(10,70,160)','white');
        for (var i = 0; i < this.nodes.length; i++) {
            this.nodes[i].draw(this.ctx);
        }
    }

    // We need atleast 4 points to start rendering the curve.
    if(this.nodes.length < 4) return;
    
    // draw all tangents
    if(this.showTangents)
        this.drawTangents();
}

// NOTE: Task 5 code.
CatmullRomSpline.prototype.drawTask5 = function()
{
    // clear the rect
    this.ctx.clearRect(0, 0, this.dCanvas.width, this.dCanvas.height);
    
    if (this.showControlPolygon) {
        // Connect nodes with a line
        setColors(this.ctx,'rgb(10,70,160)');
        for (var i = 1; i < this.nodes.length; i++) {
            drawLine(this.ctx, this.nodes[i-1].x, this.nodes[i-1].y, this.nodes[i].x, this.nodes[i].y);
        }
        // Draw nodes
        setColors(this.ctx,'rgb(10,70,160)','white');
        for (var i = 0; i < this.nodes.length; i++) {
            this.nodes[i].draw(this.ctx);
        }
    }

    // We need atleast 4 points to start rendering the curve.
    if(this.nodes.length < 4) return;
    
    // Draw the curve
    this.draw();
    
    if(this.showTangents)
        this.drawTangents();
}

// Add a contro point to the Bezier curve
CatmullRomSpline.prototype.addNode = function(x,y)
{
    this.nodes.push(new Node(x,y));
}
