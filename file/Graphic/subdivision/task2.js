function catmullClarkSubdivision(vertices, faces) {
    var newVertices = [];
    var newFaces = [];
    
    var edgeMap = {};
    // This function tries to insert the centroid of the edge between
    // vertices a and b into the newVertices array.
    // If the edge has already been inserted previously, the index of
    // the previously inserted centroid is returned.
    // Otherwise, the centroid is inserted and its index returned.
    function getOrInsertEdge(a, b, centroid) {
        var edgeKey = a < b ? a + ":" + b : b + ":" + a;
        if (edgeKey in edgeMap) {
            return edgeMap[edgeKey];
        } else {
            var idx = newVertices.length;
            newVertices.push(centroid);
            edgeMap[edgeKey] = idx;
            return idx;
        }
    }
    
    // TODO: Implement a function that computes one step of the Catmull-Clark subdivision algorithm.
    //
    // Input:
    // `vertices`: An array of Vectors, describing the positions of every vertex in the mesh
    // `faces`: An array of arrays, specifying a list of faces. Every faces is a list of vertex
    //          indices, specifying its corners. Faces may contain an arbitrary number
    //          of vertices (expect triangles, quadrilaterals, etc.)
    //
    // Output: Fill in newVertices and newFaces with the vertex positions and
    //         and faces after one step of Catmull-Clark subdivision.
    // It should hold:
    //         newFaces[i].length == 4, for all i
    //         (even though the input may consist of any of triangles, quadrilaterals, etc.,
    //          Catmull-Clark will always output quadrilaterals)
    //
    // Pseudo code follows:
    
    // ************************************
    // ************** Step 1 **************
    // ******** Linear subdivision ********
    // ************************************
    // for v in vertices:
    //      addVertex(v.clone())
    for(var i = 0 ; i < vertices.length ; i++)
        newVertices.push(vertices[i]);

    // for face in faces:
    //      facePointIndex = addVertex(centroid(face))
    //      for v1 in face:
    //          v0 = previousVertex(face, v1)
    //          v2 = nextVertex(face, v1)
    //          edgePointA = getOrInsertEdge(v0, v1, centroid(v0, v1))
    //          edgePointB = getOrInsertEdge(v1, v2, centroid(v1, v2))
    //          addFace(facePointIndex, edgePointA, v1, edgePointB)
    for(var i = 0 ; i < faces.length ; i++){
        var mod = faces[i].length;
        var faceindex = newVertices.length;
        var facecentroid = new Vector(0,0,0);
        for(var k = 0 ; k < mod ; k++)
            facecentroid = facecentroid.add(vertices[faces[i][k]]);

        newVertices.push(facecentroid.divide(mod));
        
         for(var j = 0 ; j < mod ; j++){
            var v1 = faces[i][j];
            var v0 = faces[i][(j+mod-1)%mod];
            var v2 = faces[i][(j+1)%mod];
            var edgePointA = getOrInsertEdge(v0,v1,(vertices[v0].add(vertices[v1])).divide(2.0));
            var edgePointB = getOrInsertEdge(v1,v2,(vertices[v1].add(vertices[v2])).divide(2.0));
            newFaces.push([faceindex, edgePointA, faces[i][j], edgePointB]);
         }
    }
    // ************************************
    // ************** Step 2 **************
    // ************ Averaging *************
    // ************************************
    // avgV = []
    // avgN = []
    // for i < len(newVertices):
    //      append(avgV, new Vector(0, 0, 0))
    //      append(avgN, 0)
    // for face in newFaces:
    //      c = centroid(face)
    //      for v in face:
    //          avgV[v] += c
    //          avgN[v] += 1
    //
    // for i < len(avgV):
    //      avgV[i] /= avgN[i]
    var avgV = [];
    var avgN = [];
    var zero = new Vector(0,0,0);
    for(var i = 0 ; i < newVertices.length ; i++){
        avgV.push(zero);
        avgN.push(0);
    }

    for(var i = 0 ; i < newFaces.length ; i++){
        var centroid = new Vector(0,0,0);
        for(var k = 0 ; k < 4 ; k++)
            centroid = centroid.add(newVertices[newFaces[i][k]]);
        centroid = centroid.divide(4);

        for(var k = 0 ; k < 4 ; k++){
            avgV[newFaces[i][k]] = avgV[newFaces[i][k]].add(centroid);
            avgN[newFaces[i][k]] += 1;
        }

    }

    for(var i = 0 ; i < avgV.length ; i++){
        avgV[i] =  avgV[i].divide(avgN[i]);
    }
    
    // ************************************
    // ************** Step 3 **************
    // ************ Correction ************
    // ************************************
    // for i < len(avgV):
    //      newVertices[i] = lerp(newVertices[i], avgV[i], 4/avgN[i])
    
    for(var i = 0 ; i < avgV.length ; i++)
        newVertices[i] = (newVertices[i].multiply(1-4/avgN[i])).add(avgV[i].multiply(4/avgN[i]));   
    // Do not remove this line
    return new Mesh(newVertices, newFaces);
};

function extraCreditMesh() {
    // TODO: Insert your own creative mesh here
    
    var vertices = [
        new Vector(0,  -0.5,  0),
        new Vector(-0.5, 0,  0.25),
        new Vector(-0.5, 0, -0.25),
        new Vector(-0.25, 0,  0.25),
        new Vector(-0.25, 0,  -0.25),
        new Vector(-0.125, 0.5, 0),
        new Vector(0, 0.05, +0.25),
        new Vector(0, 0.05, -0.25),
        new Vector(0.125, 0.5, 0),
        new Vector(+0.5, 0, -0.25),
        new Vector(+0.5, 0, +0.25),
        new Vector(0.25, 0, +0.25),
        new Vector(0.25, 0, -0.25)

    ];
    
    var faces = [
        [0, 1, 3],
        [0, 3, 6],
        [0, 6, 11],
        [0, 11, 10],
        [0, 2, 1],
        [0, 4, 2],
        [0, 7, 4],
        [0, 12, 7],
        [0, 10, 9],
        [0, 9, 12],
        [5, 1, 2],
        [5, 3, 1],
        [5, 6, 3],
        [5, 7, 6],
        [5, 4, 7],
        [5, 2, 4],
        [8, 11, 6],
        [8, 10, 11],
        [8, 9, 10],
        [8, 12, 9],
        [8, 7, 12],
        [8, 6, 7]
     ];

    var phiSubd = 8;
    var thetaSubd = 4;
    var radius = 0.1;
    var index = vertices.length;
    
    for (var i = 0; i <= phiSubd; ++i) {
        for (var j = 0; j <= thetaSubd; ++j) {
            var phi = i*Math.PI*2/phiSubd;
            var theta = j*Math.PI*2/thetaSubd;
            
            vertices.push(new Vector(
                Math.cos(phi)*(1.0 + Math.cos(theta)*radius),
                Math.sin(phi)*(1.0 + Math.cos(theta)*radius),
                Math.sin(theta)*radius
            ).divide(1.5));
            
            if (i > 0 && j > 0) {
                faces.push([
                    (i - 0)*(thetaSubd + 1) + (j - 0) + index,
                    (i - 0)*(thetaSubd + 1) + (j - 1) + index,
                    (i - 1)*(thetaSubd + 1) + (j - 1) + index,
                    (i - 1)*(thetaSubd + 1) + (j - 0) + index
                ]);
            }
        }
    }
    index = vertices.length;

    for (var i = 0; i <= phiSubd; ++i) {
        for (var j = 0; j <= thetaSubd; ++j) {
            var phi = i*Math.PI*2/phiSubd;
            var theta = j*Math.PI*2/thetaSubd;
            
            vertices.push(new Vector(
                Math.cos(phi)*(1.0 + Math.cos(theta)*radius),
                Math.sin(theta)*radius,
                Math.sin(phi)*(1.0 + Math.cos(theta)*radius)
            ).divide(1.5));
            
            if (i > 0 && j > 0) {
                faces.push([
                    (i - 0)*(thetaSubd + 1) + (j - 0) + index,
                    (i - 0)*(thetaSubd + 1) + (j - 1) + index,
                    (i - 1)*(thetaSubd + 1) + (j - 1) + index,
                    (i - 1)*(thetaSubd + 1) + (j - 0) + index
                ]);
            }
        }
    }
       index = vertices.length;
    for (var i = 0; i <= phiSubd; ++i) {
        for (var j = 0; j <= thetaSubd; ++j) {
            var phi = i*Math.PI*2/phiSubd;
            var theta = j*Math.PI*2/thetaSubd;
            
            vertices.push(new Vector(
                Math.sin(theta)*radius,
                Math.cos(phi)*(1.0 + Math.cos(theta)*radius),
                Math.sin(phi)*(1.0 + Math.cos(theta)*radius)
            ).divide(1.5));
            
            if (i > 0 && j > 0) {
                faces.push([
                    (i - 0)*(thetaSubd + 1) + (j - 0) + index,
                    (i - 0)*(thetaSubd + 1) + (j - 1) + index,
                    (i - 1)*(thetaSubd + 1) + (j - 1) + index,
                    (i - 1)*(thetaSubd + 1) + (j - 0) + index
                ]);
            }
        }
    }

    var wand = [
    new Vector(0.1, 0, 0.1),
    new Vector(0.1, 0, -0.1),
    new Vector(-0.1, 0, 0.1),
    new Vector(-0.1, 0, -0.1),
    new Vector(0.1, -4, 0.1),
    new Vector(0.1, -4, -0.1),
    new Vector(-0.1,-4, 0.1),
    new Vector(-0.1,-4, -0.1)
    ];

    var wandface = [
        [0, 1, 3, 2],
        [4, 5, 7, 6],
        [0, 4, 5, 1],
        [2, 6, 4, 0],
        [3, 7, 6, 2],
        [1, 5, 7, 3]

    ];
    
    index = vertices.length;
    for(var m = 0; m < wand.length ; m++)
        vertices.push(wand[m].divide(2));
    console.log(vertices.length);

    for(var i = 0; i < wandface.length ; i++){
        faces.push([
            wandface[i][0] + index,
            wandface[i][1] + index,
            wandface[i][2] + index,
            wandface[i][3] + index
            ]);
        console.log([
            wandface[i][0] + index,
            wandface[i][1] + index,
            wandface[i][2] + index,
            wandface[i][3] + index
            ]);
    }
    
    return new Mesh(vertices, faces, true);
}

var Task2 = function(gl) {
    this.pitch = 0;
    this.yaw = 0;
    this.subdivisionLevel = 0;
    this.selectedModel = 0;
    this.gl = gl;
    
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    
    this.baseMeshes = [];
    for (var i = 0; i < 6; ++i)
        this.baseMeshes.push(this.baseMesh(i).toTriangleMesh(gl));
    
    this.computeMesh();
}

Task2.prototype.setSubdivisionLevel = function(subdivisionLevel) {
    this.subdivisionLevel = subdivisionLevel;
    this.computeMesh();
}

Task2.prototype.selectModel = function(idx) {
    this.selectedModel = idx;
    this.computeMesh();
}

Task2.prototype.baseMesh = function(modelIndex) {
    switch(modelIndex) {
    case 0: return createCubeMesh(); break;
    case 1: return createTorus(8, 4, 0.5); break;
    case 2: return createSphere(4, 3); break;
    case 3: return createIcosahedron(); break;
    case 4: return createOctahedron(); break;
    case 5: return extraCreditMesh(); break;
    }
    return null;
}

Task2.prototype.computeMesh = function() {
    var mesh = this.baseMesh(this.selectedModel);
    
    for (var i = 0; i < this.subdivisionLevel; ++i)
        mesh = catmullClarkSubdivision(mesh.vertices, mesh.faces);
    
    this.mesh = mesh.toTriangleMesh(this.gl);
}

Task2.prototype.render = function(gl, w, h) {
    gl.viewport(0, 0, w, h);
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    var projection = Matrix.perspective(35, w/h, 0.1, 100);
    var view =
        Matrix.translate(0, 0, -5).multiply(
        Matrix.rotate(this.pitch, 1, 0, 0)).multiply(
        Matrix.rotate(this.yaw, 0, 1, 0));
    var model = new Matrix();
    
    if (this.subdivisionLevel > 0)
        this.baseMeshes[this.selectedModel].render(gl, model, view, projection, false, true, new Vector(0.7, 0.7, 0.7));

    this.mesh.render(gl, model, view, projection);
}

Task2.prototype.dragCamera = function(dx, dy) {
    this.pitch = Math.min(Math.max(this.pitch + dy*0.5, -90), 90);
    this.yaw = this.yaw + dx*0.5;
}
