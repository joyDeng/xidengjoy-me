var PhongVertexSource = `
    // TODO: Implement a vertex shader to do Lambert and Blinn-Phong shading
    //       Base this shader on the previous task.
    //       You will need two attributes this time (position, normal)
    //       You will need at least one varying (to pass smooth vertex normals
    //       to the fragment shader)
    //       You may need multiple uniforms to get all the required matrices
    //       for transforming points, vectors and normals. Refer to assignment 0
    //       and assignment 1 for how to correctly transform points, vectors and normals
    uniform mat4 ModelViewProjection;
    uniform mat4 ModelView;
    attribute vec3 Position;
    attribute vec3 Normal;
    attribute vec3 index;
    varying vec3 Vnormal;
    varying vec3 Pos;
    void main() {
        gl_Position = ModelViewProjection * vec4(Position,1.0);
        Vnormal = normalize((ModelView * vec4(Normal,0.0)).xyz);
        Pos = (ModelView * vec4(Position,1.0)).xyz;
    }
`;
var PhongFragmentSource = `
    precision highp float;
    
    uniform mat4 ModelView;
    uniform mat4 View;
    uniform mat4 TView;
    varying vec3 Vnormal;
    varying vec3 Pos;
    //attribute vec3 Potato;



    const vec3 LightPosition = vec3(4, 2, 4);
    const vec3 LightIntensity = vec3(20);
    const vec3 ka = 0.3*vec3(1, 0.5, 0.5);
    const vec3 kd = 0.7*vec3(1, 0.5, 0.5);
    const vec3 ks = vec3(0.4);
    const float n = 60.0;
    
    // TODO: Implement a fragment shader to do Lambert and Blinn-Phong shading
    //       The material parameters (ka, kd, ks, n) and the position- and intensity
    //       of the single light (in world coordinates) are given to you.
    //       Your code should do the same as your shading code for assignment 1,
    //       except with only a single light and without shadow rays and reflection/refraction.
    
    void main() {
        
        vec3 l = LightPosition - Pos;
        vec3 light = normalize(l);
        vec3 nVnormal = normalize(Vnormal);
        float cos = dot(light,nVnormal);
        cos = max(cos,0.0);
        vec3 Light = (kd * LightIntensity * cos)/dot(l,l);

        vec3 e = (TView * vec4(0.0,0.0,0.0,1.0)).xyz;
        vec3 v = e - Pos;
        vec3 v_n = normalize(v);
        vec3 h = light + v_n;
        vec3 h_n = normalize(h);
        float cosnh = dot(h_n,nVnormal);
        cosnh = max(cosnh,0.0);
        float cosnhn = pow(cosnh,n);
        Light = Light + ka + (ks * LightIntensity * cosnhn)/dot(l,l);
        gl_FragColor = vec4(Light,1.0);

    }
`;

function createVertexBuffer(gl, vertexData) {
    // TODO: Create a buffer, bind it to the ARRAY_BUFFER target, and
    //       copy the array `vertexData` into it
    //       Return the created buffer
    //       Commands you will need: gl.createBuffer, gl.bindBuffer, gl.bufferData
    var buf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER,buf);
    gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(vertexData),gl.STATIC_DRAW);
    return buf;

}
function createIndexBuffer(gl, indexData) {
    // TODO: Create a buffer, bind it to the ELEMENT_ARRAY_BUFFER target, and
    //       copy the array `indexData` into it
    //       Return the created buffer
    //       Commands you will need: gl.createBuffer, gl.bindBuffer, gl.bufferData
    var ind = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,ind);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexData),gl.STATIC_DRAW);
    return ind;

}
function createShaderObject(gl, shaderSource, shaderType) {
    // TODO: Create a shader of type `shaderType`, submit the source code `shaderSource`,
    //       compile it and return the shader
    //       Commands you will need: gl.createShader, gl.shaderSource, gl.compileShade
        var shader = gl.createShader(shaderType);
        gl.shaderSource(shader,shaderSource);
        gl.compileShader(shader);
        return shader;
}
function createShaderProgram(gl, vertexSource, fragmentSource) {
    var   vertexShader = createShaderObject(gl,   vertexSource, gl.VERTEX_SHADER);
    var fragmentShader = createShaderObject(gl, fragmentSource, gl.FRAGMENT_SHADER);
    
    // TODO: Create a shader program, attach `vertexShader` and `fragmentShader`
    //       to it, link the program and return the result.
    //       Commands you will need: gl.createProgram, gl.attachShader, gl.linkProgram
  var program = gl.createProgram();

// Attach pre-existing shaders
   gl.attachShader(program, vertexShader);
   gl.attachShader(program, fragmentShader);

   gl.linkProgram(program);
   return program;
}

var ShadedTriangleMesh = function(gl, vertexPositions, vertexNormals, indices, vertexSource, fragmentSource) {
    this.indexCount = indices.length;
    this.positionVbo = createVertexBuffer(gl, vertexPositions);
    this.normalVbo = createVertexBuffer(gl, vertexNormals);
    this.indexIbo = createIndexBuffer(gl, indices);
    this.shaderProgram = createShaderProgram(gl, vertexSource, fragmentSource);
}

ShadedTriangleMesh.prototype.render = function(gl, model, view, projection) {
    // TODO: Implement a render method to do Lambert- and Blinn-Phong Shading
    //       This method will closely follow the render method in assignment 1.
    //       However, this time you will need to setup two attributes (for vertex
    //       position and vertex normal). You may also need to supply multiple uniforms.
    gl.useProgram(this.shaderProgram);
    var matrix = projection.multiply(view.multiply(model));
    var modelv = view.multiply(model);
    var projectv = projection.multiply(view);
    
    var uniformlocation = gl.getUniformLocation(this.shaderProgram,"ModelViewProjection");
    gl.uniformMatrix4fv(uniformlocation,false,matrix.transpose().m); 
    
    var shadernormal = gl.getUniformLocation(this.shaderProgram,"ModelView");
    gl.uniformMatrix4fv(shadernormal,false,model.transpose().m);

    var Camera = gl.getUniformLocation(this.shaderProgram,"View");
    gl.uniformMatrix4fv(Camera,false,view.m);

    var tview = gl.getUniformLocation(this.shaderProgram,"TView");
    gl.uniformMatrix4fv(tview,false,view.inverse().transpose().m);

    gl.bindBuffer(gl.ARRAY_BUFFER,this.positionVbo);
     var location = gl.getAttribLocation(this.shaderProgram,"Position");
    gl.enableVertexAttribArray(location);
    gl.vertexAttribPointer(location, 3, gl.FLOAT, false, 0, 0);
    
    gl.bindBuffer(gl.ARRAY_BUFFER,this.normalVbo);
    var normal = gl.getAttribLocation(this.shaderProgram,"Normal");
    gl.enableVertexAttribArray(normal);
    gl.vertexAttribPointer(normal, 3, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this.indexIbo);
    //var ind = getAttribLocation(this.shaderProgram,"index");
    //gl.enableVertexAttribArray(ind);
    //gl.vertexAttribPointer(ind, 3, gl.FLOAT, false, 0, 0);


    gl.drawElements(gl.TRIANGLES,this.indexCount,gl.UNSIGNED_SHORT,0);

}

var Task2 = function(gl) {
    this.cameraAngle = 0;
    this.sphereMesh = new ShadedTriangleMesh(gl, SpherePositions, SphereNormals, SphereIndices, PhongVertexSource, PhongFragmentSource);
    this.cubeMesh = new ShadedTriangleMesh(gl, CubePositions, CubeNormals, CubeIndices, PhongVertexSource, PhongFragmentSource);
    
    gl.enable(gl.DEPTH_TEST);
}

Task2.prototype.render = function(gl, w, h) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    var projection = Matrix.perspective(60, w/h, 0.1, 100);
    var view =
        Matrix.translate(0, 0, -5).multiply(
        Matrix.rotate(this.cameraAngle, 1, 0, 0));
    var rotation = Matrix.rotate(Date.now()/25, 0, 1, 0);
    var cubeModel = Matrix.translate(-1.8, 0, 0).multiply(rotation);
    var sphereModel = Matrix.translate(1.8, 0, 0).multiply(rotation).multiply(Matrix.scale(1.2, 1.2, 1.2));

    this.sphereMesh.render(gl, sphereModel, view, projection);
    this.cubeMesh.render(gl, cubeModel, view, projection);
}

Task2.prototype.dragCamera = function(dy) {
    this.cameraAngle = Math.min(Math.max(this.cameraAngle + dy*0.5, -90), 90);
}
